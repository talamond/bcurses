# Bitburner Curses

This is a library for more advanced and flexible display functions for 
Bitburner log windows.

While there are multiple other display libraries out there, BCurses aims to fit
a niche that they do not, namely:
* Does not interrupt the games aesthetic (as it generates no new html elements
and uses only ingame functions to display (it's all wrapped around ns.print)
* It's portable, as this library does not require installing anything in the 
game environment, it can be handled as either a single file or imported via 
link.
* The goal of BCurses is to be really easy to use, it's not a production 
javascript display library bolted on to Bitburner

(The name is a reference to the ncurses library for TUI displays, much like a
lot of Bitburner in-game tools, this is only a homage and bears no relationship
to the original projects)

## Basic Quickstart

The basic structure of bcurses is around creating windows to display and render
within the log window. After that, it's core function is being able to 
arbitrarily place text wherever you want in the window.

An demo application is present in the main function of the library. 

Important notes about usage: 

As this library makes the log into more of a 
display than a text feed, it does not dynamically resize with the window.

Additionally, attaching child windows should be noted as having a performance
impact. Each child attached increases the workload each time the window 
renders.

### Basic Window (curse_window)

**Note:** All other window types are extensions of this

**Constructor:** `curse_window(ns, border=false, height=27, width=51)`

Setting border to true will reduce the draw area available but will put a
text pipe border around the whole window.

Default width is what I found to be the character width of my log windows. You
can set this to any value, setting wider than the window will require you to
resize it to fit. Setting wider than a parent window will result in content
being cut off.

`addChild(handle, x, y)` - This accepts another window as the handle and the
x,y coordinates of where you want it to appear within this window.

Child windows appear on top of the parent window and do not interfere with the
content of the parent window.

`getSubDimensions()` - This returns a simple array containing 
`[width, height]`. This represents the dimensions of the drawing area. If no
border is set, the sub dimensions will match the window dimensions.

`setPosition(x,y)` - This is used to change the location of the window within
it's parent window.

`setDimensions(height, width)` - This will clear the windows contents and
change it's dimensions.

`addText(x,y, text)` - This function will place arbitrary text in the window.
This text will remain until overwritten or erased with a `clearBuffer()`.

`render(clear=false, refresh=true)` - This function wraps around 
`clearBuffer()`, `refreshBuffer()`, and `drawToLog()` to conveniently update
the display in one command.

`rendersleep(time, clear=false)` - Returns a promise and waits either time in
milliseconds or until the render is done, whichever comes *last*. Clear is
passed through to the render function.

`renderToString(clear=false, refresh=true)` - Returns a promise that delivers
a string containing what would normally be output to the log.

`drawToLog()` - Immediately prints the display to the log window without
updating any child windows.

`clearBuffer()` - Resets the buffer to be blank (with border if set)

`refreshBuffer(print=true)` - Clears the log window if print is set and the
window is the parent window. Triggers a render on all child windows and updates
the buffer to include the output of those windows as well.

### Scrolling Text (curse_stream)

This extends the curse_window object to provide a scrolling text window with
minimal processor overhead. This object mostly exists to allow the default
behavior of the log to be mostly kept while still being able to draw on top.

Default Height for this window is set to 100 to match the default log length.

**Note:** While print is the primary way of accessing this window, `addText()`
is still available. Any text added with `addText()` will scroll up whenever
`print()` is run.

`print(input)` - Prints the output to the window scrolling bottom to top. If
this is the only window this will look similar to the default behavior of the
log with `ns.print()`

`renderOnPrint` - A variable containing a boolean. If true, whenever `print()`
is run it will run a render as well to immediately update the log.

### Progress Bar (curse_progress)

This is a minimal module that expects a height of 1 (3 with border, 2 with
partial border).

`update(progress)` - Takes a float between 0.0 and 1.0. Will dynamically draw a
progress bar across the window mapping the progress to the width of the window.